let t = 300;
let g = 300;
let timer1;

function animation() {
    const layer = document.getElementById("star");
    layer.style.left = t+"px";
    layer.style.top = g+"px";
}

function startAnimation() {
    const layer = document.getElementById("star");
    layer.style.visibility="visible";
    timer1 = setInterval(animation,20);
}

function stopAnimation() {
    clearInterval(timer1);
}

function init() {
    startAnimation();
}
window.addEventListener("load", init);

let lastMove = 0;

function onMouseMove (e) {
  t = e.clientX;
  g = e.clientY;
  
  lastMove = Date.now();
}
window.addEventListener('mousemove', onMouseMove);