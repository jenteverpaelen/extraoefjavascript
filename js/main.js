function getParameter(name) {
    const queryString = decodeURIComponent(location.search.replace(/\+/g, "%20"));
    const regex = new RegExp(name + "=([^&]+)");
    const result = regex.exec(queryString);
    if (result)
        return result[1];
    else
        return null;
}



function init() {
    let gr = document.getElementById("welcomeHeader");
    const firstName = getParameter("voorNaam");
    const lastName = getParameter("familieNaam");
    if (firstName === null || lastName === null) {
        firstName = "";
        lastName = "";
    } else {
        gr.innerHTML = "Hello " + firstName + " " + lastName;
    }
}

window.addEventListener("load", init);