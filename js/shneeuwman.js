let x = 1500;
let y = 1500;
let timer;

function animation() {
    const layer = document.getElementById("santa");
    if((--x == 0) || (--y == 0)) {
        stopAnimation();
    }
    layer.style.left=x+"px";
    layer.style.right=y+"px";

}

function startAnimation() {
    const layer = document.getElementById("santa");
    layer.style.visibility="visible";
    layer.style.left=x+"px";
    layer.style.right=y+"px";
    timer = setInterval(animation,20);
}

function stopAnimation() {
    const layer = document.getElementById("santa");
    clearInterval(timer);
    layer.style.visibility="hidden";
}

function init() {
    setTimeout(startAnimation, 3000);
}
window.addEventListener("load", init);